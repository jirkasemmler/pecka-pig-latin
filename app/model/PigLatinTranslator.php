<?php

namespace App\Model;

class PigLatinTranslator {

    private $suffix = 'ay';
    private $delimiter = '-';
    private $extraConsonant = 'w';

    /**
     * @param string $delimiter
     * @return PigLatinTranslator
     */
    public function setDelimiter(string $delimiter): PigLatinTranslator {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * @param string $extraConsonant
     * @return PigLatinTranslator
     */
    public function setExtraConsonant(string $extraConsonant): PigLatinTranslator {
        $this->extraConsonant = $extraConsonant;
        return $this;
    }

    /**
     * @param string $suffix
     * @return PigLatinTranslator
     */
    public function setSuffix(string $suffix): PigLatinTranslator {
        $this->suffix = $suffix;
        return $this;
    }


    /**
     * translates string to PigLatin form
     * @param string $inputString
     * @return string
     */
    public function translate(string $inputString): string {

        // creates list of words to be translated and list of white chars between them
        $words = preg_split("/\s+/i", $inputString);
        $spaces = preg_split("/\S+/i", $inputString);

        // translating the words
        $words = $this->translateWords($words);
        // merging words with their glue
        return $this->matchWordsAndWhiteChars($words, $spaces);

    }

    /**
     * @param string[] $words
     * @return string[] array
     */
    private function translateWords($words) {
        $out = [];
        foreach ($words as $word) {
            if (preg_match("/^[aeiuo]+.*/i", $word, $output_array)) {
                $out[] = preg_replace("/([aeiou]+[a-z0-9]*)/i",
                    "$1{$this->delimiter}{$this->extraConsonant}{$this->suffix}", $word);

            } else {
                if (preg_match("/^y.*/i", $word, $output_array)) {
                    $out[] = preg_replace("/([bcdfghjklmnpqrstvwxyz]+)([aeiou]+[a-z0-9]*)/i",
                        "$2{$this->delimiter}$1{$this->suffix}", $word);;

                } else {
                    $out[] = preg_replace("/([bcdfghjklmnpqrstvwxz]+)([aeiouy]+[a-z0-9]*)/i",
                        "$2{$this->delimiter}$1{$this->suffix}", $word);;
                }
            }
        }
        return $out;

    }

    private function matchWordsAndWhiteChars($words, $whiteChars) {
        $i = 0;
        $outString = '';
        foreach ($words as $word) {
            $outString .= $whiteChars[$i] . $word;
            $i++;
        }
        $outString .= $whiteChars[$i];
        return $outString;
    }
}