<?php

namespace App\Presenters;

use Nette;
use Nette\Application\Responses;
use Tracy\ILogger;


final class ErrorPresenter implements Nette\Application\IPresenter {
    use Nette\SmartObject;

    /** @var ILogger */
    private $logger;


    public function __construct(ILogger $logger) {
        $this->logger = $logger;
    }


    public function run(Nette\Application\Request $request) {
        $exception = $request->getParameter('exception');

        return new Responses\JsonResponse(['error' => $exception->getMessage()]);
    }
}
