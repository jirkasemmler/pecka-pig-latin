<?php

namespace App\Presenters;

use App\Model\PigLatinTranslator;
use Nette;

class ApiPresenter extends Nette\Application\UI\Presenter {

    /**
     * @SWG\Post(
     *     path="/pig-latin",
     *     description="Translates input text to PigLatin form",
     *     consumes={"text/plain"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="dialect-suffix",
     *         in="formData",
     *         description="Suffix used in PigLatin. default ay",
     *         required=false,
     *         type="string",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="dialect-delimiter",
     *         in="formData",
     *         description="Delimiter between word and suffix. Default -",
     *         required=false,
     *         type="string",
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="dialect-extraConsonant",
     *         in="formData",
     *         type="string",
     *         description="Consonant used for words ending with vowel. Default w",
     *         required=false,
     *         @SWG\Schema(type="string"),
     *     ),
     *     @SWG\Parameter(
     *         name="text",
     *         in="formData",
     *         type="string",
     *         description="Text to be translated",
     *         required=true,
     *         @SWG\Schema(type="string"),
     *     ),
     *  @SWG\Response(
     *     response=200,
     *     description="Translated text",
     *     @SWG\Schema(type="string"),
     *   ),
     * )
     */
    public function actionPigLatin() {
        $translator = new PigLatinTranslator();
        $postData = $this->getHttpRequest()->getPost();
        if (isset($postData['dialect-suffix'])) {
            $translator->setSuffix($postData['dialect-suffix']);
        }
        if (isset($postData['dialect-delimiter'])) {
            $translator->setDelimiter($postData['dialect-delimiter']);
        }
        if (isset($postData['dialect-extraConsonant'])) {
            $translator->setExtraConsonant($postData['dialect-extraConsonant']);
        }

        if (isset($postData['text'])) {
            $this->sendJson(['text' => $translator->translate($postData['text'])]);
        } else {
            throw new Nette\Application\BadRequestException('invalid data');
        }
    }
}