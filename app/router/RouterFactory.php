<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Drahak\Restful\Application\Routes\ResourceRoute;


final class RouterFactory {
    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter() {
        $router = new RouteList;


        $router[] = new ResourceRoute('pig-latin[.<type xml|json>]',
            ['presenter' => 'Api', 'action' => 'pigLatin'],
            ResourceRoute::POST
        );

        return $router;
    }
}
