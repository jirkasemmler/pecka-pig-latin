# pecka-pig-latin

PeckaDesign test task. 

Pig-Latin task implemented as POST REST API /latin-pig
4 parameters
- dialect-suffix : string, defines suffix used in translating the words, default 'ay'
- dialect-delimiter : string, defines the string between original word and siffux. Default -
- dialect-extraConsonant : string - defines string between vowel and suffix
- text : string - text to be translated