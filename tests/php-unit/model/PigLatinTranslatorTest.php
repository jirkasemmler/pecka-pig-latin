<?php

use App\Model\PigLatinTranslator;
use PHPUnit\Framework\TestCase;

require __DIR__ . '/../../../vendor/autoload.php';

class PigLatinTranslatorTest extends TestCase {

    /**
     * @var PigLatinTranslator
     */
    private $translator;

    public function setUp() {
        $this->translator = new PigLatinTranslator;
    }

    /**
     * @dataProvider textsProvider
     */
    public function testTranslate($in, $exp) {
        $this->assertEquals($exp, $this->translator->translate($in));
    }

    /**
     * @dataProvider textsProvider
     */
    public function testTranslateWithCustomSuffix($in, $exp) {
        $this->translator->setSuffix('omg');
        $exp = str_replace('ay', 'omg', $exp);
        $this->assertEquals($exp, $this->translator->translate($in));
    }

    /**
     * @dataProvider textsProvider
     */
    public function testTranslateWithCustomDelimiter($in, $exp) {
        $this->translator->setDelimiter("'");
        $exp = str_replace('-', "'", $exp);
        $this->assertEquals($exp, $this->translator->translate($in));
    }


    public function testTranslateWithCustomExtraConsonant() {
        $this->translator->setExtraConsonant("p");
        $data = $this->textsProvider();
        $exp = str_replace('way', 'pay', $data['vowelStart']['out']);
        $this->assertEquals($exp, $this->translator->translate($data['vowelStart']['in']));
    }


    public function textsProvider() {
        $path = implode(DIRECTORY_SEPARATOR, [__DIR__, 'fixtures', 'textsToTranslate.json']);
        $out = json_decode(file_get_contents($path), true);
        return $out;
    }
}